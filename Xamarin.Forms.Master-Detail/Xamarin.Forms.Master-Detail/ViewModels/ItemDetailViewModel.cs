﻿using System;

using Xamarin.Forms.Master_Detail.Models;

namespace Xamarin.Forms.Master_Detail.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.Text;
            Item = item;
        }
    }
}
